/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt4e10aldarias;

/**
 * Fichero: Ifelseanidado.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */

public class Ifelseanidado {
  public static void main (String args[]) {
    int nota=6;
    if (nota<5) {
      System.out.println("Mal");
    } else if (nota==5) {
      System.out.println("Regular");
    } else if (nota>5) {
      System.out.println("Bien");
    }
  }
}
/* EJECUCION:
Bien
*/