
package prgt4e10aldarias;

/**
 * Fichero: DoWhile01.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */

public class DoWhile01 {

  public static void main(String args[]) {
    int contador;
    contador = 1; // inicializar
    do {
      System.out.print(contador + " ");
      contador++;
    } while (contador < 6);
  }
}

/* EJECUCION:
1 2 3 4 5
*/